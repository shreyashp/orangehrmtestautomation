package org.test.loginpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.test.Providers;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class LoginTest {
    @Test(priority = 0)
    public void browserTest() {
        try {
            Providers.webDriver = new ChromeDriver();
            Providers.webDriver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

        } catch (Exception e0) {
            System.out.println(e0);
            try {
                Providers.webDriver
                        = new FirefoxDriver();
                Providers.webDriver
                        .get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

            } catch (Exception e1) {
                System.out.println(e1);
                try {
                    Providers.webDriver
                            = new EdgeDriver();
                    Providers.webDriver
                            .get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

                } catch (Exception e2) {
                    System.out.println(e2);
                    Assert.fail("Supported Browser is not available in your machine");
                }
            }
        }
    }

    @Test(priority = 1)
    public void textBoxTest() throws InterruptedException {
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getPageSource().contains("Login"));
        WebElement username = Providers.webDriver.findElement(By.name("username"));
        Assert.assertTrue(username.isDisplayed());
        WebElement password = Providers.webDriver.findElement(By.name("password"));
        Assert.assertTrue(password.isDisplayed());

        username.sendKeys("Admin");
        password.sendKeys("admin123");

        WebElement btn = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button"));
        btn.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getPageSource().contains("Dashboard"));
        Assert.assertTrue(Providers.webDriver.getPageSource().contains("Dashboard"));
    }
}
