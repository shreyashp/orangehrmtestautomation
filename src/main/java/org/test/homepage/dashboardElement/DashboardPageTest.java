package org.test.homepage.dashboardElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.test.Providers;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class DashboardPageTest {
    @Test(priority = 0)
    void scrollTest() throws InterruptedException {
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getPageSource().contains("Dashboard"));
        new Actions(Providers.webDriver).scrollByAmount(
               0, 15000
        ).perform();
        Thread.sleep(Duration.ofSeconds(2));
        new Actions(Providers.webDriver).scrollByAmount(
                0, -15000
        ).perform();


    }
    @Test(priority = 1)
    void buttonsTest() throws InterruptedException {
        //Time button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[1]/div/div[2]/div[1]/div[2]/button")).isDisplayed());
        WebElement timeButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[1]/div/div[2]/div[1]/div[2]/button"));
        Assert.assertTrue(timeButton.isDisplayed());
        timeButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/attendance"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/attendance"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //Assign button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/button")).isDisplayed());
        WebElement assignButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/button"));
        Assert.assertTrue(assignButton.isDisplayed());
        assignButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/leave"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/leave"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //Leave List button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[2]/button")).isDisplayed());
        WebElement leaveListButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[2]/button"));
        Assert.assertTrue(leaveListButton.isDisplayed());
        leaveListButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("leave/viewLeaveList"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("leave/viewLeaveList"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //Timesheet button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[3]/button")).isDisplayed());
        WebElement timeSheetButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[3]/button"));
        Assert.assertTrue(timeSheetButton.isDisplayed());
        timeSheetButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/time/viewEmployeeTimesheet"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/time/viewEmployeeTimesheet"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //Apply Leave button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[4]/button")).isDisplayed());
        WebElement applyLeaveButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[4]/button"));
        Assert.assertTrue(applyLeaveButton.isDisplayed());
        applyLeaveButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/leave/applyLeave"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/leave/applyLeave"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //My Leave button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[5]/button")).isDisplayed());
        WebElement myLeaveButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[5]/button"));
        Assert.assertTrue(myLeaveButton.isDisplayed());
        myLeaveButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/leave/viewMyLeaveList"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/leave/viewMyLeaveList"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();

        //My Timesheet button test
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[6]/button")).isDisplayed());
        WebElement myTimeSheetButton = Providers.webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[6]/button"));
        Assert.assertTrue(myTimeSheetButton.isDisplayed());
        myTimeSheetButton.click();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.getCurrentUrl().contains("/time/viewMyTimesheet"));
        Assert.assertTrue(Providers.webDriver.getCurrentUrl().contains("/time/viewMyTimesheet"));
        Thread.sleep(Duration.ofSeconds(2));
        Providers.webDriver.navigate().back();
        new WebDriverWait(Providers.webDriver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/div/div[6]/button")).isDisplayed());
    }
}
